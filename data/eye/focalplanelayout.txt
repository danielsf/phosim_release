# focal plane geometry layout data
# 
# rows are the chips in the focal plane
# columns are the name, x position (microns), y position (microns),
# pixel size (microns), number of x pixels, number of y pixels,
# type of device (note CMOS also means Fast Frame CCD),
# readout time (CCD) or frame rate (CMOS)
# group of sensors, 3 euler rotations (degrees), 3 translations (mm)
# deformation type, deformation coefficients, QE variation (obsolete)
#
chip          0.0       0.0 40.0  1024  1024 CCD    3.0 100.0 Group0     0.000000   0.000000   0.000000  0.000000   0.000000   0.000000 zern      0.0  0.0  0.0 0.00  0.00  0.00 0.00 0.00  0.00 0.00 0.00 0.00  0.00 0.00  0.00 0.00 0.00  0.00 0.00 0.00 0.00  0.00
