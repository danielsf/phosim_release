# Implemented from Arizona model with accommodation A = 0
#
# Column 0: Name
# Column 1: Type
# Column 2: Curvature R (mm)
# Column 3: Thickness dz (mm)
# Column 4: Outer Radius (mm)
# Column 5: Inner Radius (mm)
# Column 6: Conic Constant Kappa
# Column 7 - 14: Aspheric Coefficient a_3 - a_10 (a_n r^n in meters)
# Column 15: Coating file
# Column 16: Medium file
#
# (0)	(1)	(2)     	(3)     (4)     (5)	(6)     (7)     (8)     (9)	(10)    (11)    (12)    (13)	(14)    (15)    (16)                                                         
cornea1	lens	.01282		0.0	6.0	0.0	-0.25	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	cornea_dispersion.txt
cornea2	lens	-.1538		0.55	6.0	0.0	-0.25	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	aqueous_dispersion.txt
iris	lens	0.0		2.97	2.566	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	aqueous_dispersion.txt
lens1	lens	-.08333 	0.0	4.9	0.0	-7.519	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	lens_dispersion.txt
lens2	lens	.1914		3.767	4.9	0.0	-1.354	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	vitreous_dispersion.txt
retina	det	0.0		16.713	15	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	vacuum
