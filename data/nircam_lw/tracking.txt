# tracking information
# windjitter is rms angle of wind jitter (degrees) after 1 minute of drifting
# rotation jitter is rms arcseconds of rotation jitter at visit time
# elevation jitter is the arcseconds of elevation jitter at visit time
# azimuth jitter is the arcsecond of azimuth jitter at visit time
windjitter 0.0
rotationjitter 0.0
elevationjitter 0.0
azimuthjitter 0.0
