#Usabilty: ./phosim data/generic/star -c examples/perfect -i generic
#
# Column 0: Name
# Column 1: Type
# Column 2: Curvature R (mm)
# Column 3: Thickness dz (mm)
# Column 4: Outer Radius (mm)
# Column 5: Inner Radius (mm)
# Column 6: Conic Constant Kappa
# Column 7 - 14: Aspheric Coefficient a_3 - a_10 (a_n r^n in meters)
# Column 15: Coating file
# Column 16: Medium file
#
# (0)	(1)	(2)     (3)     (4)     (5)	(6)     (7)     (8)     (9)	(10)    (11)    (12)    (13)	(14)    (15)    (16)                                                         
primary	mirror	8000	0.0	2000	0.0	-1.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	vacuum
image	det	0.0	4000	15	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	none	vacuum
