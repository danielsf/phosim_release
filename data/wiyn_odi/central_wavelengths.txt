# approximate data for optimizations for each filter configuration (column 1)
# column 2 is the nominal wavelength of each band (microns)
# column 3 is the approximate plate scale (microns per degree)
# columns 4 5 6 are the x y z coordinates of the center of the image plane (microns)
# columns 7 8 9 are the Euler rotations of the image plane (degrees)
0   0.5  392727.272727 0.0 0.0 0.0 0.0 0.0 0.0