//-*-mode:c++; mode:font-lock;-*-
///
/// @package phosim
/// @file sg_lsst_buzzard_file.h
/// @brief Class to read/write/access the records of the an entry of the LSST
///         Buzzard galaxy catalog
///         ref: 	http://www.slac.stanford.edu/~jderose/buzzard-
///                                          highres/buzzard-highres_v1.0.html
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
#ifndef SGLSSTBUZZARDFILE_H
#define SGLSSTBUZZARDFILE_H

#include "stdint.h"
#include <cmath>
#include <string>
#include <valarray>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>

#include <fitsio.h>

using namespace std;

// *************************************************************************
// Index to "names" of variablers in both truth and LSSTMag tables
// Lables defined in constructor and placed in vector <string > SGBuzzardName
// *************************************************************************
enum SGBuzzardElements { BZ_INDEX, BZ_ECATID, BZ_Z, BZ_RA, BZ_DEC, BZ_TE, 
						 BZ_TSIZE, BZ_OMAG};
 
// *************************************************************************
// Data structure to hold one galaxy(note we may need to add bulge/disk stuff)
// (Follows Buzzard "Catalog Tags) from http://www.slac.stanford.edu/
//                ~jderose/buzzard-highres/buzzard-highres_v1.0_lssttags.html
// **********************************************************************

class SGBuzzardGalaxy
{
 public:
  int    id;       //A unique id # for that galaxy
  int    ecatid;   //Index SDSS galaxy from the training-set
  double z;        //True galaxy redshift.
  double RADeg;    //Galaxy position.
  double decDeg;   //Galaxy position.
  double te;       //Simulated Galaxy Ellipticity
  double tsize;    //Simulated Galaxy Size (FLUX_RADIUS), in arcseconds.
  double omag;     //The observed LSST magnitude (needs some checking to be
                   //what we want !!!!!!!!#########!)
  //Probably will want to add SED specfication (however we do it).
};

// ************************************************************************

class SGBuzzardLSSTFile
// ***********************************************************************
// This class creates/opens/closes/reads/writes the LSST Buzzard catalog
// ref:http://www.slac.stanford.edu/~jderose/buzzard-
//                                          highres/buzzard-highres_v1.0.html
// ***********************************************************************
{
 public:
  SGBuzzardLSSTFile();
  virtual ~SGBuzzardLSSTFile();

  void   OpenBuzzardFitsFiles( std::string TruthCatalogFileName);
  bool   GetNextGalaxy(SGBuzzardGalaxy& galaxy);

  double getPresentRow(){return fPresentRow;};

  void DetermineColumnNum();
  std::string DeriveLSSTMagFileName(std::string TruthCatalogFileName);
  std::string GetLSSTMagFileName(){return fBuzzardLSSTMagFileName;};

 private:
  std::string fBuzzardLSSTMagFileName;
  int    fId;
  int    fEcatid;
  float  fRADeg;
  float  fDecDeg;
  float  fZ;
  float  fTe;
  float  fTsize;
  float  fOmag;

  int fitsStatus;
  fitsfile* pfTruthFile;
  fitsfile* pfLSSTMagFile;

  long fNumRowsInFile;
  long fPresentRow;
  std::vector < string > fColumnName;      //loaded in constructor
  std::vector < int >    fTruthColumnNum;  //loaded in OpenBuzzardFitsFiles

};
#endif



