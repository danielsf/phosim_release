//-*-mode:c++; mode:font-lock;-*-
///
/// @package phosim
/// @file sunmoon.h
/// @brief  Functions related to the sun and moon
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Created by:
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
// *************************************************************************
// Most of this follows algorithms using slalib derived from moon.c by
// Robert Jedicke. 
// see ifa.hawaii.edu/users/jedicke/cutility/html/moon_8h-source.html
// *************************************************************************

#ifndef SUNMOON_H
#define SUNMOON_H

#include <cstdlib>   //For definition of NULL
#include "pal.h"
#include "palmac.h"
#include "erfa.h"
#include "erfam.h"

class SunMoon
{
 public: 
  SunMoon();
  virtual ~SunMoon();
  void   MoonOfDate ( double MJD, double* pMoonRA2000, double* pMoonDec2000);
  void   Moon2000   ( double MJD, double* pMoonRA2000, double* pMoonDec2000);
  double MoonPhase  ( double MJD, double* pSunRA2000,  double* pSunDec2000,
					              double* pMoonRA2000, double* pMoonDec2000);
  void   Sun2000    ( double MJD, double* pSunRA2000,  double* pSunDec2000);  
};
#endif /* SUNMOON_H */
