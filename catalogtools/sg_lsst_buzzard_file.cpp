//-*-mode:c++; mode:font-lock;-*-
///
/// @package phosim
/// @file SGGalaxyCatalogFile.cpp
/// @brief Class to read/write/access the records of the an entry of the LSST
///        Buzzard galaxy catalogs
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author 
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "sg_lsst_buzzard_file.h"


SGBuzzardLSSTFile::SGBuzzardLSSTFile()
{
  // *******************************************************************
  // Initalize names of the columns
  // ******************************************************************
  int numNames = BZ_OMAG +1;
  fColumnName.clear();
  fColumnName.resize(numNames);
  fColumnName.at(BZ_INDEX) ="INDEX";
  fColumnName.at(BZ_ECATID) ="ECATID";
  fColumnName.at(BZ_Z) ="Z";
  fColumnName.at(BZ_RA) ="RA";
  fColumnName.at(BZ_DEC) ="DEC";
  fColumnName.at(BZ_TE) ="TE";
  fColumnName.at(BZ_TSIZE) ="TSIZE";
  fColumnName.at(BZ_OMAG) ="OMAG";
  
  fTruthColumnNum.clear();
  fTruthColumnNum.resize(numNames-1);
  

 //Nothing here yet;
}
// **********************************************************************

SGBuzzardLSSTFile::~SGBuzzardLSSTFile()
{
  // Nothing here yet
}
// ***********************************************************************

void SGBuzzardLSSTFile::OpenBuzzardFitsFiles(std::string TruthCatalogFileName)
// *********************************************************************
// The Buzzard galaxy catalog for LSST consists of two complimentary files.
// The first is the base "Truth" catalog fits file. This has all but AMAG: The
// absolute galaxy magnitude in LSST uGRIZ Y2 Y3 Y4 bands, where Y4 is the 
// default Y band. and OMAG: The observed LSST magnitudes.magnitudes. These 
// are instead in the LSSTMagnitude files.see:
// http://www.slac.stanford.edu/~jderose/buzzard-highres/buzzard-highres_v1.0_
// lssttags.html
// Contact: 
// Joe Derose
// jderose@stanford.edu
// *********************************************************************
// These files are fits files with the data in  the extended tables.
// Open both files and prepare for use.
// We are using the standard cfitsio library.
// *********************************************************************
{
  cout << "-Opening Input Truth Fits file: "
	   << TruthCatalogFileName << endl;
  fitsStatus = 0;
  if (fits_open_table(&pfTruthFile, TruthCatalogFileName.c_str(), READONLY, 
					 &fitsStatus)) {
	cout<<"Error opening fits table in file : " << TruthCatalogFileName 
		<< std::endl;
	exit(1);
  }
  // **********************************************
  //Fnd column number for the colums we want.
  // **********************************************
  DetermineColumnNum();

  std::string BuzzardLSSTMagFileName = 
                                 DeriveLSSTMagFileName( TruthCatalogFileName);
  cout << "-Opening Input LSST Magnitude Fits file: "
	   << BuzzardLSSTMagFileName << endl;

  fitsStatus = 0;
  if (fits_open_table(& pfLSSTMagFile, BuzzardLSSTMagFileName.c_str(), 
					 READONLY, &fitsStatus)) {
	cout<<"Error opening fits table in file : " <<  BuzzardLSSTMagFileName 
		<< std::endl;
	exit(1);
  }
 
  fits_get_num_rows(pfTruthFile, &fNumRowsInFile, &fitsStatus);

  cout << "Number of rows in Truth Table: " << fNumRowsInFile
	   << endl;
  fPresentRow = 0;  //Rows start at 1?


  // ******************************
  // And we are ready to read it in line by line
  // ******************************
  return;
}
// ************************************************************************
 
bool SGBuzzardLSSTFile::GetNextGalaxy( SGBuzzardGalaxy& galaxyRecord)
// ************************************************************************* 
// Read a galaxy from the Buzzard input files and place into the 
// galaxyRecord vector.
// The calling routine can use the SGBuzzardElements enum to find particular 
// elements in the data vector.
// *************************************************************************
{
  // *********************************************************
  // Check if we are done
  // *********************************************************
  fPresentRow++;
  if(fPresentRow > fNumRowsInFile ) {
	return false;
  }
  
  // ***********************************************************************
  // Fits tables have mixed types.
  // ***********************************************************************

 
  //  read the columns , one row at a time 
  int   anyNull   = 0;
  long  numElem   = 1;
  int   intNull   = 0;
  float floatNull = 0.0;

  fits_read_col(pfTruthFile, TINT, fTruthColumnNum.at(BZ_INDEX), fPresentRow,
				numElem, numElem, &intNull, &fId, &anyNull, &fitsStatus);
  fits_read_col(pfTruthFile, TINT, fTruthColumnNum.at(BZ_ECATID), fPresentRow,
				numElem, numElem, &intNull, &fEcatid, &anyNull, &fitsStatus);

  fits_read_col(pfTruthFile, TFLOAT, fTruthColumnNum.at(BZ_Z), fPresentRow,
				numElem, numElem, &floatNull, &fZ, &anyNull, &fitsStatus);
  fits_read_col(pfTruthFile, TFLOAT, fTruthColumnNum.at(BZ_RA), fPresentRow,
				numElem, numElem, &floatNull, &fRADeg, &anyNull, &fitsStatus);
  fits_read_col(pfTruthFile, TFLOAT, fTruthColumnNum.at(BZ_DEC), fPresentRow,
				numElem, numElem, &floatNull, &fDecDeg, &anyNull, &fitsStatus);
  fits_read_col(pfTruthFile, TFLOAT, fTruthColumnNum.at(BZ_TE), fPresentRow,
				numElem, numElem, &floatNull, &fTe, &anyNull, &fitsStatus);
  fits_read_col(pfTruthFile, TFLOAT, fTruthColumnNum.at(BZ_TSIZE), fPresentRow,
				numElem, numElem, &floatNull, &fTsize, &anyNull, &fitsStatus);

  fits_read_col(pfLSSTMagFile, TFLOAT, 1, fPresentRow, numElem, numElem,
				&floatNull, &fOmag, &anyNull, &fitsStatus);

  
  galaxyRecord.id     = fId;
  galaxyRecord.ecatid = fEcatid;
  galaxyRecord.z      = fZ;
  galaxyRecord.RADeg  = fRADeg;
  galaxyRecord.decDeg = fDecDeg;
  galaxyRecord.te     = fTe;
  galaxyRecord.tsize  = fTsize;
  galaxyRecord.omag   = fOmag;    //Use G(?)order is u,G,R,I,Z,Y2,Y3,Y4

  return true;
}
// *************************************************************************


std::string  SGBuzzardLSSTFile::DeriveLSSTMagFileName(
                                            std::string TruthCatalogFileName)
{
  // **********************************************************************
  // Derive form the Truth Catalog file name the companion LSST magnitude file
  // name. Mainly we just need to extract the HELAPix value. Star at the end 
  // of the file name and move back File name stcture:
  // "Buszzard-highres_galaxies_shmatch." + HEALPix + ".fits"
  // **********************************************************************
  //Get position of character after HEALPix
  // **************************************
  std::string::size_type idx =  TruthCatalogFileName.find_last_of(".");
  if ( idx == std::string::npos ) {
	cout << "Buzzard-highres HEALPix file name: " <<  TruthCatalogFileName
		 << "has bad format: " << std::endl;
	exit(1);
  }

  // *********************************************************
  // Chop off the end of the file name starting at last "."
  // *********************************************************
  std::string HEALPix = TruthCatalogFileName.substr(0,idx-1);
  
  // *******************************************************************
  // Find character before start of HEALPix
  // *******************************************************************
  idx =  HEALPix.find_last_of(".");
  if ( idx == std::string::npos ) {
	cout << "Buzzard-highres HeEALPix file name: " <<  TruthCatalogFileName
		 << "has bad format: " << std::endl;
	exit(1);
  }

  // ****************************************************************
  // Get the HEALPix
  // ****************************************************************
  HEALPix=HEALPix.substr(idx+1);

  // ***************************************************************
  // Now build the LSST mag file name
  // ****************************************************************
  fBuzzardLSSTMagFileName = "Buzzard-highres_LSST." + HEALPix + ".fits";
  return fBuzzardLSSTMagFileName;
}
// ************************************************************************

void SGBuzzardLSSTFile::DetermineColumnNum()
// ********************************************************************
// Once we have the fits file opened to the table determine the column 
// numbers for all the variables we need
// ********************************************************************
{
  // Truth file  table first
  int numNames = BZ_OMAG;
  fitsStatus=0;
  for (int i = 0; i < numNames ; i++) {
	fits_get_colnum(pfTruthFile, CASESEN, (char*)fColumnName.at(i).c_str(),
					 &fTruthColumnNum.at(1), &fitsStatus);
  }
  return;
}
// ***********************************************************************
