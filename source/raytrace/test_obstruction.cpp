///
/// @package phosim
/// @file test_obstruction.cpp
/// @brief Unit tests for obstruction class.
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @brief Modified by:
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "validation/unittest.h"

int main() {
    return 0;
}
