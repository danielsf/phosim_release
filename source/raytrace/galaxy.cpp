///
/// @package phosim
/// @file galaxy.cpp
/// @brief galaxy photon sampler
///
/// @brief Created by:
/// @author Suzanne Lorenz (Purdue)
///
/// @brief Modified by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "ancillary/random.h"
#include "constants.h"
#include "helpers.h"
#include "parameters.h"
#include "galaxy.h"

double Galaxy::sampleSersic(char *sersicdata) {
    int i;
    int n;
    char temp[20];
    float b_n[99];
    double t = 0;
    double sum = 0;
    FILE *fp;

    fAll = static_cast<double**>(calloc(99, sizeof(double*)));
    for (n = 0; n < 99; n++) {
        fAll[n] = static_cast<double*>(calloc(8000, sizeof(double)));
    }
    cAll = static_cast<double**>(calloc(99, sizeof(double*)));
    for (n = 0; n < 99; n++) {
        cAll[n] = static_cast<double*>(calloc(8000, sizeof(double)));
    }
    dAll = static_cast<double*>(calloc(8000, sizeof(double)));

    fp = fopen(sersicdata, "r");
    for (i = 0; i < 99; i++) {
        fgets(temp, 20, fp);
        sscanf(temp, "%f", &b_n[i]);
    }
    for (n = 0; n < 99; n++) {
        t = 0.0;
        sum = 0.0;
        for (i = 0; i < bin_number; i++) {
            fAll[n][i] = t*t*exp(-b_n[n]*(pow(t, 1/((n + 1)/scale)) - 1));
            cAll[n][i] = fAll[n][i] + sum;
            sum = cAll[n][i];
            dAll[i] = t;
            t = t + 0.01;
        }
        for (i = 0; i < bin_number; i++) {
            cAll[n][i] = cAll[n][i]/sum;
        }
    }

    return 0;
}

int Galaxy::sersic(double a, double b, double c, double alpha, double beta,
                   double n, double *x_out, double *y_out) {

    double rand_az = random.uniform();
    double theta = M_PI*2*(rand_az);
    double rand_z = random.uniform();
    double z_axis = 2*(rand_z - 0.5);
    double randomValue = random.uniform();
    double dummy = n*scale;
    int nn = (int) (dummy);
    long q = 0 ;
    find(*(cAll + nn), bin_number, randomValue, &q);//use n here
    int down = q;
    int up = q + 1;
    double frak = (randomValue - cAll[nn][down])/(cAll[nn][up] - cAll[nn][down]);
    double r_a = a*(dAll[down]*(1 - frak) + dAll[up]*frak);
    double r_b = b*(dAll[down]*(1 - frak) + dAll[up]*frak);
    double r_c = c*(dAll[down]*(1 - frak) + dAll[up]*frak);
    double x = sqrt(1 - pow(z_axis, 2))*cos(theta)*r_a;
    double y = sqrt(1 - pow(z_axis, 2))*sin(theta)*r_b;
    z_axis = z_axis*r_c;
    //double x_prime = x*cos(alpha)+z_axis*sin(alpha);
    double z_prime = x*sin(alpha) + z_axis*cos(alpha);
    double dist = sqrt(pow(z_prime, 2.0) + pow(y, 2.0));
    if (y > 0) {
        dist = -dist;
    }
    double gamma = atan(z_prime/y);
    double angle = beta + gamma;
    double f = dist*(cos(angle));
    double d = dist*(sin(angle));
    *x_out = f;
    *y_out = d;
    return 0;
}

double Galaxy::sampleSersic2d(char *sersicdata) {
    int i;
    int n;
    char temp[20];
    float b_n[99];
    double t = 0;
    double sum = 0;
    FILE *fp;

    fAll2d = static_cast<double**>(calloc(99, sizeof(double*)));
    for (n = 0; n < 99; n++) {
        fAll2d[n] = static_cast<double*>(calloc(8000, sizeof(double)));
    }
    cAll2d = static_cast<double**>(calloc(99, sizeof(double*)));
    for (n = 0; n < 99; n++) {
        cAll2d[n] = static_cast<double*>(calloc(8000, sizeof(double)));
    }
    dAll2d = static_cast<double*>(calloc(8000, sizeof(double)));

    fp = fopen(sersicdata, "r");
    for (i = 0; i < 99; i++) {
        fgets(temp, 20, fp);
        sscanf(temp, "%f", &b_n[i]);
    }
    for (n = 0; n < 99; n++) {
        t = 0.0;
        sum = 0.0;
        for (i = 0; i < bin_number; i++) {
            fAll2d[n][i] = t*exp(-b_n[n]*(pow(t, 1/((n + 1)/scale)) - 1));
            cAll2d[n][i] = fAll2d[n][i] + sum;
            sum = cAll2d[n][i];
            dAll2d[i] = t;
            t = t + 0.01;
        }
        for (i = 0; i < bin_number; i++) {
            cAll2d[n][i] = cAll2d[n][i]/sum;
        }
    }

    return 0;
}

int Galaxy::sersic2d(double a, double b, double beta, double n, double *x_out, double *y_out) {

    double theta = M_PI*2*(random.uniform());
    double randomValue = random.uniform();
    int nn = (int)(n*scale);
    long q = 0;
    find(*(cAll2d + nn), bin_number, randomValue, &q);
    double r = interpolate(dAll2d, *(cAll2d + nn), randomValue, q);
    double x = cos(theta)*a*r;
    double y = sin(theta)*b*r;
    double dist = sqrt(x*x + y*y);
    if (y > 0) {
        dist = -dist;
    }
    double angle = beta + atan(x/y);
    *x_out = dist*(cos(angle));
    *y_out = dist*(sin(angle));
    return 0;
}
