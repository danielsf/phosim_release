"""
  @package
  @file phosimVisualizer.py
  @brief plot the optical digram
  @brief Created by:
  @author Jun Cheng (Purdue), En-Hsin (Purdue)
  @brief Modified by:
  @author Colin Burke (Purdue)
  @warning This code is not fully validated
  and not ready for full release.  Please
  treat results with caution.
Usage: see README.md file
Notes: 1. Plot only part of the photons with 'percentage'
"""
import os,sys
import numpy as np
from astropy.io import fits
from mayavi import mlab

controls = [] #This global variable is probably bad practice, but I can't think of an easy way to get rid of it

class photon(object):
    def  __init__(self):
        self.wavelength = 0
        self.numLayer = 0
        self.xdir = 0
        self.ydir = 0
        self.time = 0
        self.listX = []
        self.listY = []
        self.listZ = []
        self.listLayer = []
        self.opticX = []
        self.opticY = []
        self.opticZ = []

    def printPhoton(self):
        print "****************************PHOTON*********************************"
        print "wavelength " , self.wavelength
        print "X-direction ", self.xdir
        print "Y-direction ", self.ydir
        print "time ", self.time
        print "number of Layers ", self.numLayer
        print "*******"
        for i in range(0, self.numLayer):
            print self.listLayer[i], self.listX[i], self.listY[i], self.listZ[i]
        #print "******************************************************************"

class Control(object): #Really not fully validated
    def __init__(self, line):
        self.name = line[0]
        self.type = line[1]
        self.parameter = float(line[4]) #mm (decenter) or rad (tilt)
        self.links = []  #surface links -- surfaces onto which we apply the control
        for i in range(11, len(line)):
            self.links.append(int(line[i])) #Possible bug: comments at the end of line
    def applyControl(self, actor): #Apply rotation or translation control
        if self.type == "xdis":
            actor.position = [self.parameter, actor.position[1], actor.position[2]]
        elif self.type == "ydis":
            actor.position = [actor.position[0], self.parameter, actor.position[2]]          
        elif self.type == "zdis":
            actor.position = [actor.position[0], actor.position[1], self.parameter]
        #control.txt rotations -> vtk orientations
        elif self.type == "phi":
            pass
            #do stuff
        elif self.type == "psi":
            pass
            #do stuff
        elif self.type == "theta":
            pass
            #do stuff
        
class Surface(object): 
    # surface constructor: read one line for a specific surface
    def __init__(self, line, surfaceNumber): 
        self.name = line[0]
        self.type = line[1]
        self.curvature = float(line[2])
        self.thickness = float(line[3])
        self.outerRadius = float(line[4])
        self.innerRadius = float(line[5])
        self.conic = float(line[6])
        self.aspheric3 = float(line[7])
        self.aspheric4 = float(line[8])
        self.aspheric5 = float(line[9])
        self.aspheric6 = float(line[10])
        self.aspheric7 = float(line[11])
        self.aspheric8 = float(line[12])
        self.aspheric9 = float(line[13])
        self.aspheric10 = float(line[14])
        self.coatingFile = line[15]
        self.mediaFile = line[16]
        self.surfaceNumber = surfaceNumber #surface number (used to link control.txt)
    def plotSurface(self, globalZ): 
        if(self.type!="det"):# or self.name=="lens"): 
            r, theta = np.mgrid[self.innerRadius:self.outerRadius:100j, -np.pi:np.pi:100j]
            x = r*np.cos(theta)
            y = r*np.sin(theta)

            r = np.sqrt(x**2 + y**2)
            if self.curvature<1.0e-12:
                z = globalZ + r*0
            else:
                z = globalZ + r**2/(self.curvature*(1+np.sqrt(1-(1+self.conic)*r**2/(self.curvature**2))))+r**3*self.aspheric3 + r**4*self.aspheric4
            if self.type=="filter": 
                mesh = mlab.mesh(x,y,z, opacity=0.3, color = (1,1,0))
            elif self.type=="none":
                mesh = mlab.mesh(x,y,z, opacity=0.05, color=(1,1,1))
            else:
                mesh = mlab.mesh(x,y,z, opacity=1.0, color=(0.6,0.9,0.9), colormap="Pastel2")
            
            for control in controls:
                for link in control.links:
                    if link == self.surfaceNumber:
                        control.applyControl(mesh.actor.actor)
            #mlab.show()
                
class Chip(object): 
        def __init__(self,line): 
            self.name = line[0]
            self.centerX = float(line[1])   # unit micro
            self.centerY = float(line[2])   # unit micro
            self.pixelSize = float(line[3])
            self.numX = float(line[4])
            self.numY = float(line[5])

            self.halfX = self.pixelSize *self.numX/2
            self.halfY = self.pixelSize *self.numY/2
        def plotChip(self, pos): 
            x, y = np.mgrid[-self.halfX:self.halfX:10j, -self.halfY:self.halfY:10j]
            x = (x+self.centerX)/1000
            y = (y+self.centerY)/1000
            z= x-x+pos
            mlab.mesh(x,y,z, opacity = 0.95, color=(0.1,0.1,0.1))
            #mlab.show()

def updatePath(oldPath): 
    # if it is relative path, convert to full path
    newPath = oldPath
    if not os.path.exists(oldPath): 
        newPath = os.path.join(os.path.dirname(__file__), oldPath)
    return newPath


def readConfig(fileName):
    f = open(fileName,"r")
    config = {}
    eventFileList = []
    for line in f:
        if line[0]!='#':# and not line.strip():
            tok = line.split()
            if len(tok)<2:
                continue
            if(tok[0]=="eventFile"):                 
                eventFileList.append(updatePath(tok[1]))
            else:
                config[tok[0]] = tok[1]
    config["eventFile"] = eventFileList


    if config["instrumentPath"][-1]!="/":       
        config["instrumentPath"] += "/"
    path = config["instrumentPath"]
    if "opticsFile" in config:
        config["opticsFile"] = path + config["opticsFile"] 
    if "focalplaneFile" in config:
        config["focalplaneFile"] = path + config["focalplaneFile"]
    if "segmentationFile" in config:
        config["segmentationFile"] = path + config["segmentationFile"]
    if "controlFile" in config:
        config["controlFile"] = path + config["controlFile"] 

    return config

def readMultpleEvents(config): 
    # read multiple event file
    for eventFits in config["eventFile"]: 
        readEvents(eventFits, config["percentage"])


def readEvents(eventFits, per):
    # read a single event file; 
    #eventFits = config["eventFile"] #"output.fits"
    #per = config["percentage"]
    f= fits.open(eventFits)
    event=f[1].data
    #print event.field
    xpos = []
    ypos = []
    zpos = []
    wavelength = []
    numLine = len(event.field(0))
    # set a small number for test.
    numLine = 1000
    numPhoton = 0
    photonList = []
    pre_p = photon()
    for i in range(0, numLine):
        if event.field(3)[i]==0:

            if i!=0:
                pre_p.numLayer=numLayer
                photonList.append(pre_p)
            numLayer = 0
            numPhoton += 1
            p = photon()
            p.wavelength = event.field(2)[i]
            p.xdir = event.field(0)[i]
            p.ydir = event.field(1)[i]
        elif event.field(3)[i]==1:
            p.time = event.field(0)[i]
        else :
            numLayer += 1
            p.listX.append(event.field(0)[i])
            p.listY.append(event.field(1)[i])
            p.listZ.append(event.field(2)[i])
            p.listLayer.append(event.field(3)[i])

        pre_p = p

    photonList.append(p)

    plotPhoton= int(float(per) * numPhoton)
    print plotPhoton, "out of", numPhoton

    for i in range(0,plotPhoton):
        length = len(photonList[i].listZ)
        if length > 8 :
            for n in range(8,length-4): #I remove this '-4' to draw photons that don't make it all the way
                mlab.plot3d(photonList[i].listX[n:2+n], photonList[i].listY[n:2+n],photonList[i].listZ[n:2+n],color=(1, 0.5, 0.5),
                        opacity=0.2, tube_radius=None)
    #mlab.show()

def readControls(config): 
    controlFile = config["controlFile"]
    for line in open(controlFile).readlines():
        if line[0]!="#" and line.strip()!="":
            temp = line.split()
            controls.append(Control(temp))

def readOptics(config): 
    opticsFile = config["opticsFile"]
    height=0.0
    surfaces = []
    surfno = 0
    for line in open(opticsFile).readlines():
        if line[0]!= "#":
            temp = line.split()
            surfaces.append(Surface(temp, surfno))
            surfno+=1
    globalZ=0
    for i in range(len(surfaces)): 
        globalZ += surfaces[i].thickness
        surfaces[i].plotSurface(globalZ)
    return globalZ

def readChips(config, detPosition): 
    layoutFile = config["focalplaneFile"]
    ampFile = config["segmentationFile"]
    chips = []
    for line in open(layoutFile).readlines():
        if line[0]!="#":
            temp = line.split()
            chips.append(Chip(temp))
    for i in range(len(chips)):
        chips[i].plotChip(detPosition)  

def main():
    #configFileName = "/Users/cheng109/work/EventPlot/configuration.txt"
    detPosition = 0 
    configFileName = sys.argv[1]
    config = readConfig(configFileName)
    if "controlFile" in config:
        readControls(config)
    if "opticsFile" in config:
        detPosition = readOptics(config)
    if "focalplaneFile" in config:
        readChips(config, detPosition)
    if "eventFile" in config: 
        readMultpleEvents(config)
    mlab.show()
    
if __name__ == "__main__":
        main()
